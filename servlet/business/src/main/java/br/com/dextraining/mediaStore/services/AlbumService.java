package br.com.dextraining.mediaStore.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import br.com.dextraining.mediaStore.entities.Album;
import br.com.dextraining.mediaStore.entities.Musica;
import br.com.dextraining.mediaStore.utils.EntityManagerUtil;

public class AlbumService extends ServiceImpl<Long, Album> {

	public AlbumService() {
		super(Album.class);
	}

	public Album buscarPorIdComMusicas(Long id) {
		String consulta = "FROM Album a";
		consulta += " JOIN FETCH a.musicas";
		consulta += " WHERE a.id = :id";

		EntityManager em = EntityManagerUtil.criarEntityManager();
		TypedQuery<Album> query = em.createQuery(consulta, Album.class);
		query.setParameter("id", id);

		Album album = query.getSingleResult();
		em.close();
		return album;
	}

	public Album buscarPorIdCriteria(Long id) {
		EntityManager em = EntityManagerUtil.criarEntityManager();

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Album> criteria = criteriaBuilder
				.createQuery(Album.class);

		Root<Album> root = criteria.from(Album.class);
		root.fetch("musicas");
		criteria.select(root);
		criteria.where(criteriaBuilder.equal(root.get("id"), id));


		TypedQuery<Album> query = em.createQuery(criteria);
		Album singleResult = query.getSingleResult();
		em.close();
		return singleResult;
	}

	public List<Album> buscarPorTrechoMusica(String trecho) {
		String consulta = "FROM Album a";
		consulta += " JOIN FETCH a.musicas m";
		consulta += " WHERE m.titulo LIKE :titulo";

		EntityManager em = EntityManagerUtil.criarEntityManager();
		TypedQuery<Album> query = em.createQuery(consulta, Album.class);
		query.setParameter("titulo", "%" + trecho + "%");

		List<Album> resultList = query.getResultList();
		em.close();
		return resultList;
	}
	
	public List<Album> albunsPorArtista(Long idArtista) {
		String consulta = "SELECT a FROM Album a, Artista artista";
		consulta += " WHERE artista.id = :id";
		consulta += " AND a MEMBER OF artista.albuns";
		
		EntityManager em = EntityManagerUtil.criarEntityManager();
		TypedQuery<Album> query = em.createQuery(consulta, Album.class);
		query.setParameter("id", idArtista);
		List<Album> resultList = query.getResultList();
		em.close();
		return resultList;
	}

}
