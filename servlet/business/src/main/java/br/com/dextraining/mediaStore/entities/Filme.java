package br.com.dextraining.mediaStore.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue(value="FILME")
public class Filme extends Produto {

	@Column
	private String diretor;
	
	@ManyToMany
	private List<Categoria> categorias;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="filme")
	private List<FilmeAtor> filmeAtores;

	public String getDiretor() {
		return diretor;
	}

	public void setDiretor(String diretor) {
		this.diretor = diretor;
	}

	public List<Categoria> getCategorias() {
		if(this.categorias == null) {
			this.categorias = new ArrayList<Categoria>();
		}
		return categorias;
	}

	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}

	public List<FilmeAtor> getFilmeAtores() {
		if(this.filmeAtores == null) {
			this.filmeAtores = new ArrayList<>();
		}
		return filmeAtores;
	}

	public void setFilmeAtores(List<FilmeAtor> atores) {
		this.filmeAtores = atores;
	}

	public String getTipoProduto() {
		return Filme.class.getSimpleName();
	}
}
