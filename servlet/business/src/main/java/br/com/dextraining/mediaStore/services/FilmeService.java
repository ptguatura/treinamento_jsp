package br.com.dextraining.mediaStore.services;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.dextraining.mediaStore.entities.Filme;
import br.com.dextraining.mediaStore.utils.EntityManagerUtil;

public class FilmeService extends ServiceImpl<Long, Filme> {
	public FilmeService() {
		super(Filme.class);
	}
	
	public Filme buscarPorIdComAtores(Long id) {
		String consulta = "FROM Filme f";
		consulta += " LEFT JOIN FETCH f.filmeAtores";
		consulta += " WHERE f.id = :id";
		
		EntityManager em = EntityManagerUtil.criarEntityManager();
		TypedQuery<Filme> query = em.createQuery(consulta, Filme.class);
		query.setParameter("id", id);
		
		Filme filme = query.getSingleResult();
		em.close();
		return filme;
	}
}
