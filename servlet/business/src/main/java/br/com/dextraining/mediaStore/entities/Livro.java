package br.com.dextraining.mediaStore.entities;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue(value="LIVRO")
public class Livro extends Produto {

	@Column
	private String isbn;
	
	@Column
	private Integer anoPublicacao;
	
	@ManyToOne
	private Editora editora;

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public Integer getAnoPublicacao() {
		return anoPublicacao;
	}

	public void setAnoPublicacao(Integer anoPublicacao) {
		this.anoPublicacao = anoPublicacao;
	}

	public Editora getEditora() {
		return editora;
	}

	public void setEditora(Editora editora) {
		this.editora = editora;
	}
	
	public String getTipoProduto() {
		return Livro.class.getSimpleName();
	}
}
