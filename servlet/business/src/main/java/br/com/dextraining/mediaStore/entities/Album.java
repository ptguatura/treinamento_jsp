package br.com.dextraining.mediaStore.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue(value="ALBUM")
public class Album extends Produto {

	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "album")
	private Set<Musica> musicas;

	public Set<Musica> getMusicas() {
		if (this.musicas == null) {
			this.musicas = new HashSet<Musica>();
		}
		return musicas;
	}

	public void setMusicas(Set<Musica> musicas) {
		this.musicas = musicas;
	}

	public String getTipoProduto() {
		return Album.class.getSimpleName();
	}
}
