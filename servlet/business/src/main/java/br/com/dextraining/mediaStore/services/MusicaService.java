package br.com.dextraining.mediaStore.services;

import br.com.dextraining.mediaStore.entities.Musica;

public class MusicaService extends ServiceImpl<Long, Musica> {
	public MusicaService() {
		super(Musica.class);
	}
}
