package br.com.dextraining.mediaStore.services;

import br.com.dextraining.mediaStore.entities.Categoria;

public class CategoriaService extends ServiceImpl<Long, Categoria>{

	public CategoriaService() {
		super(Categoria.class);
	}
}
