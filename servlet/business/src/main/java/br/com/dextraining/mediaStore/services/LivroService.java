package br.com.dextraining.mediaStore.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import br.com.dextraining.mediaStore.entities.Editora;
import br.com.dextraining.mediaStore.entities.Livro;
import br.com.dextraining.mediaStore.utils.EntityManagerUtil;

public class LivroService extends ServiceImpl<Long, Livro>{

	public LivroService() {
		super(Livro.class);
	}
	
	public List<Livro> buscarPorIsbn(String isbn) {
		EntityManager em = EntityManagerUtil.criarEntityManager();
		
		String jpql = "FROM Livro l";
		jpql += " WHERE l.isbn LIKE :pIsbn";
		jpql += " ORDER BY l.isbn";
		
		TypedQuery<Livro> query = em.createQuery(jpql, Livro.class);
		query.setParameter("pIsbn", isbn + "%");
		
		List<Livro> resultList = query.getResultList();
		em.close();
		return resultList;
	}
	
	public List<Livro> buscarPorDescricaoEAnoLancamento(String descricao, 
			Integer ano) {
		EntityManager em = EntityManagerUtil.criarEntityManager();
		
		StringBuilder jpql = new StringBuilder("FROM Livro l");
		jpql.append(" WHERE l.descricao = :descricao");
		jpql.append(" AND l.ano = :ano");
		
		TypedQuery<Livro> query = em.createQuery(jpql.toString(), Livro.class);
		query.setParameter("descricao", descricao + "%");
		query.setParameter("ano", ano);
		
		List<Livro> resultList = query.getResultList();
		em.close();
		return resultList;
	}
	
	public List<Livro> buscarPorEditora(Editora ed) {
		EntityManager em = EntityManagerUtil.criarEntityManager();
		
		String consulta = "FROM Livro l";
		consulta += " WHERE l.editora = :editora";
		
		TypedQuery<Livro> query = em.createQuery(consulta, Livro.class);
		query.setParameter("editora", ed);
		
		List<Livro> resultList = query.getResultList();
		em.close();
		return resultList;
	}
	
	public List<Livro> buscarPorNomeEditora(String nomeEditora) {
		EntityManager em = EntityManagerUtil.criarEntityManager();
		
		String consulta = "FROM Livro l";
		consulta += " WHERE l.editora.nome LIKE :nome";
		
		TypedQuery<Livro> query = em.createQuery(consulta, Livro.class);
		query.setParameter("nome", "%" + nomeEditora + "%");
		
		List<Livro> resultList = query.getResultList();
		em.close();
		return resultList;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> buscarTituloPreco() {
		EntityManager em = EntityManagerUtil.criarEntityManager();
		
		String consulta = "SELECT l.descricao, l.valor";
		consulta += " FROM Livro l";
		
		Query query = em.createQuery(consulta);
		
		List<Object[]> resultList = query.getResultList();
		em.close();
		return resultList;
	}
	
	public List<Livro> listComCriteria() {
		EntityManager em = EntityManagerUtil.criarEntityManager();
		
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Livro> criteria = 
				criteriaBuilder.createQuery(Livro.class);
		
		Root<Livro> root = criteria.from(Livro.class);
		
		criteria.select(root);
		criteria.where(criteriaBuilder.equal(root.get("isbn"), "0002"));
		
		TypedQuery<Livro> query = em.createQuery(criteria);
		
		return query.getResultList();
	}
}
