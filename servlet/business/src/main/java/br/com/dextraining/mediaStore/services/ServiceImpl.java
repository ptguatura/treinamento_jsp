package br.com.dextraining.mediaStore.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import br.com.dextraining.mediaStore.utils.EntityManagerUtil;

public abstract class ServiceImpl<T, C> implements ServiceInterface<T, C> {
	protected Class<C> entityClass;
	
	public ServiceImpl(Class<C> entityClass) {
		this.entityClass = entityClass;
	}
	
	@Override
	public C persist(C entity) {
		EntityManager em = EntityManagerUtil.criarEntityManager();
		EntityTransaction transaction = null;
		
		try {
			transaction = em.getTransaction();
			transaction.begin();
			em.persist(entity);
			transaction.commit();
		} catch (RuntimeException e) {
			if(transaction != null && transaction.isActive()) {
				transaction.rollback();
			}
		} finally {
			em.close();
		}
		
		return entity;
	}

	@Override
	public C merge(C entity) {
		EntityManager em = EntityManagerUtil.criarEntityManager();
		EntityTransaction transaction = null;
		
		try {
			transaction = em.getTransaction();
			transaction.begin();
			em.merge(entity);
			transaction.commit();
		} catch (RuntimeException e) {
			if(transaction != null && transaction.isActive()) {
				transaction.rollback();
			}
		} finally {
			em.close();
		}
		
		return entity;
	}

	@Override
	public void remove(C entity) {
		EntityManager em = EntityManagerUtil.criarEntityManager();
		EntityTransaction transaction = null;
		
		try {
			transaction = em.getTransaction();
			transaction.begin();
			entity = em.merge(entity);
			em.remove(entity);
			transaction.commit();
		} catch (RuntimeException e) {
			if(transaction != null && transaction.isActive()) {
				transaction.rollback();
			}
		} finally {
			em.close();
		}
	}

	@Override
	public C findById(T id) {
		C entity;
		EntityManager em = EntityManagerUtil.criarEntityManager();
		entity = em.find(entityClass, id);
		em.close();
		return entity;
	}
	
	@Override
	public List<C> findAll() {
		EntityManager em = EntityManagerUtil.criarEntityManager();
		String queryStr = "FROM " + entityClass.getSimpleName();
		TypedQuery<C> query = em.createQuery(queryStr, entityClass);
		List<C> resultList = query.getResultList();
		em.close();
		return resultList;
	}
	
}
