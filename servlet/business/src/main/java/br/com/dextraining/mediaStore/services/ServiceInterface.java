package br.com.dextraining.mediaStore.services;

import java.util.List;

public interface ServiceInterface<T, C> {

	public C persist(C entity);
	
	public C merge(C entity);
	
	public void remove(C entity);
	
	public C findById(T id);
	
	public List<C> findAll();
}
