package br.com.dextraining.mediaStore.services;

import br.com.dextraining.mediaStore.entities.Ator;

public class AtorService extends ServiceImpl<Long, Ator> {
	public AtorService() {
		super(Ator.class);
	}
}
