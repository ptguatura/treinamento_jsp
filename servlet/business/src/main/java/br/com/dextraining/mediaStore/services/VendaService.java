package br.com.dextraining.mediaStore.services;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.dextraining.mediaStore.entities.ItemVenda;
import br.com.dextraining.mediaStore.entities.Pessoa;
import br.com.dextraining.mediaStore.entities.Produto;
import br.com.dextraining.mediaStore.entities.Venda;
import br.com.dextraining.mediaStore.utils.EntityManagerUtil;

public class VendaService extends ServiceImpl<Long, Venda> {
	public VendaService() {
		super(Venda.class);
	}

	public BigDecimal totalPorComprador(Pessoa comprador) {
		EntityManager em = EntityManagerUtil.criarEntityManager();

		String consulta = "SELECT SUM(v.valorTotal) FROM Venda v";
		consulta += " WHERE v.comprador = :comprador";

		Query query = em.createQuery(consulta);
		query.setParameter("comprador", comprador);

		BigDecimal total = (BigDecimal) query.getSingleResult();
		em.close();
		return total;
	}

	public BigDecimal totalPorCompradorNativeQuery(Long idComprador) {
		EntityManager em = EntityManagerUtil.criarEntityManager();

		String consulta = "SELECT SUM(v.valortotal) FROM Venda v";
		consulta = " WHERE v.comprador_id = ?";

		Query query = em.createNativeQuery(consulta);
		query.setParameter(1, idComprador);

		BigDecimal total = (BigDecimal) query.getSingleResult();
		em.close();
		return total;
	}

	public Venda vender(Pessoa comprador, List<Produto> produtos) {
		Venda venda = new Venda();
		venda.setComprador(comprador);

		for (Produto produto : produtos) {
			ItemVenda itemVenda = new ItemVenda();
			itemVenda.setProduto(produto);
			itemVenda.setValor(produto.getValor());
			venda.getItens().add(itemVenda);
		}

		venda.setDataVenda(new Date());

		venda.atualizaTotal();

		return this.persist(venda);
	}
}
