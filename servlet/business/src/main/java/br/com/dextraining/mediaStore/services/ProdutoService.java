package br.com.dextraining.mediaStore.services;

import java.math.BigDecimal;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.Query;

import br.com.dextraining.mediaStore.entities.Produto;
import br.com.dextraining.mediaStore.utils.EntityManagerUtil;

public class ProdutoService extends ServiceImpl<Long, Produto> {
	public ProdutoService() {
		super(Produto.class);
	}
	
	public void corrigirPrecos(double fatorCorrecao) {
		String consulta = "UPDATE Produto p";
		consulta += " SET p.valor = p.valor * :fator";
		
		EntityManager em = EntityManagerUtil.criarEntityManager();
		
		em.getTransaction().begin();
		Query query = em.createQuery(consulta);
		query.setParameter("fator", new BigDecimal(fatorCorrecao));
		query.executeUpdate();
		em.getTransaction().commit();
	}
	
	public void removerProdutosAbaixoPreco(BigDecimal precoLimite) {
		String consulta = "DELETE FROM Produto p";
		consulta += " WHERE p.valor < :valorLimite";
		
		EntityManager em = EntityManagerUtil.criarEntityManager();
		
		em.getTransaction().begin();
		Query query = em.createQuery(consulta);
		query.setParameter("valorLimite", precoLimite);
		query.executeUpdate();
		em.getTransaction().commit();
	}
	
	public void corrigirPrecoProduto(Long idProduto, double fator) {
		EntityManager em = EntityManagerUtil.criarEntityManager();
		
		Produto produto = findById(idProduto);
		// Travar o objeto para leitura e escrita
		em.lock(produto, LockModeType.PESSIMISTIC_WRITE);
		produto.setValor(produto.getValor().multiply(new BigDecimal(fator)));
		em.persist(produto);
		em.close();
	}
}
