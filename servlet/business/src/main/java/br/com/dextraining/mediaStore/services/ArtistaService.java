package br.com.dextraining.mediaStore.services;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.dextraining.mediaStore.entities.Artista;
import br.com.dextraining.mediaStore.utils.EntityManagerUtil;

public class ArtistaService extends ServiceImpl<Long, Artista> {
	public ArtistaService() {
		super(Artista.class);
	}
	
	public Artista buscarDiscografiaCompleta(Long id) {
		String consulta = "FROM Artista a";
		consulta += " JOIN FETCH a.albuns album";
		consulta += " JOIN FETCH album.musicas musica";
		
		EntityManager em = EntityManagerUtil.criarEntityManager();
		
		TypedQuery<Artista> query = em.createQuery(consulta, Artista.class);
		
		Artista artista = query.getSingleResult();
		em.close();
		return artista;
	}
}
