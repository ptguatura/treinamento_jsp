package br.com.dextraining.mediaStore.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.dextraining.mediaStore.entities.Pessoa;
import br.com.dextraining.mediaStore.utils.EntityManagerUtil;

public class PessoaService extends ServiceImpl<Long, Pessoa> {

	public PessoaService() {
		super(Pessoa.class);
	}
	
	public List<Pessoa> pessoasPorEstado(String estado) {
		String consulta = "FROM Pessoa p";
		consulta += " WHERE p.endereco.estado = :estado";
		
		EntityManager em = EntityManagerUtil.criarEntityManager();
		TypedQuery<Pessoa> query = em.createQuery(consulta, Pessoa.class);
		query.setParameter("estado", estado);
		
		List<Pessoa> resultado = query.getResultList();
		em.close();
		return resultado;
	}
	
	public List<Pessoa> findAllComEndereco() {
		String consulta = "SELECT DISTINCT p FROM Pessoa p";
		consulta += " JOIN FETCH p.enderecos";
		
		EntityManager em = EntityManagerUtil.criarEntityManager();
		TypedQuery<Pessoa> query = em.createQuery(consulta, Pessoa.class);
		
		List<Pessoa> resultado = query.getResultList();
		em.close();
		return resultado;
	}
}
