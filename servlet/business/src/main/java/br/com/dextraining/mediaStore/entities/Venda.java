package br.com.dextraining.mediaStore.entities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;



@Entity
public class Venda {
	
	private final static String SEQ_NAME = "SEQ_VENDA_ID";

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator=Venda.SEQ_NAME)
	@SequenceGenerator(name=Venda.SEQ_NAME, sequenceName=Venda.SEQ_NAME, allocationSize=10)
	private Long id;
	
	@ManyToOne
	private Pessoa comprador;
	
	@Column
	private Date dataVenda;
	
	@JoinColumn(name = "venda_id")
	@OneToMany(cascade= CascadeType.ALL)
	private List<ItemVenda> itens;
	
	@Column(precision=10, scale=2)
	private BigDecimal valorTotal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pessoa getComprador() {
		return comprador;
	}

	public void setComprador(Pessoa comprador) {
		this.comprador = comprador;
	}

	public Date getDataVenda() {
		return dataVenda;
	}

	public void setDataVenda(Date dataVenda) {
		this.dataVenda = dataVenda;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public List<ItemVenda> getItens() {
		if(itens == null) {
			itens = new ArrayList<ItemVenda>();
		}
		return itens;
	}

	public void setItens(List<ItemVenda> itens) {
		this.itens = itens;
	}
	
	@Transient
	public BigDecimal atualizaTotal() {
		BigDecimal total = BigDecimal.ZERO;
		for (ItemVenda item : this.itens) {
			total = total.add(item.getValor());
		}
		this.setValorTotal(total);
		return total;
	}
}
