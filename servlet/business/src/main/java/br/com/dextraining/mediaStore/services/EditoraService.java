package br.com.dextraining.mediaStore.services;

import br.com.dextraining.mediaStore.entities.Editora;

public class EditoraService extends ServiceImpl<Long, Editora> {

	public EditoraService() {
		super(Editora.class);
	}

}
