package br.com.dextraining.mediaStore.entities;

public enum Genero {
	ACAO, COMEDIA, SUSPENSE, DRAMA, TERROR
}
