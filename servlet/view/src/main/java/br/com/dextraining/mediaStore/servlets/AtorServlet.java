package br.com.dextraining.mediaStore.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dextraining.mediaStore.entities.Ator;
import br.com.dextraining.mediaStore.services.AtorService;

public class AtorServlet extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String nomeAtor = req.getParameter("nome-ator");
		Ator ator = new Ator();
		ator.setNome(nomeAtor);
		new AtorService().persist(ator);
		resp.sendRedirect("/sucesso.jsp");
	}
	

}
