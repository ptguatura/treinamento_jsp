package br.com.dextraining.mediaStore.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dextraining.mediaStore.entities.Categoria;
import br.com.dextraining.mediaStore.services.CategoriaService;

public class CategoriaServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)	throws ServletException, IOException {
		String nomeCategoria = req.getParameter("nome-categoria");
		Categoria categoria = new Categoria();
		categoria.setNome(nomeCategoria);
		CategoriaService categoriaService = new CategoriaService();
		categoriaService.persist(categoria);
		resp.sendRedirect("/sucesso.jsp");
	}

}










