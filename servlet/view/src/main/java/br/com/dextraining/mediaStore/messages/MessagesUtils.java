package br.com.dextraining.mediaStore.messages;

import java.util.Locale;
import java.util.ResourceBundle;

public class MessagesUtils {

	public static String getString(String message) {
		ResourceBundle bundle = ResourceBundle.getBundle("messages");
		return bundle.getString(message);
	}

}
