package br.com.dextraining.mediaStore.servlets;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.dextraining.mediaStore.entities.Ator;
import br.com.dextraining.mediaStore.entities.Categoria;
import br.com.dextraining.mediaStore.entities.Filme;
import br.com.dextraining.mediaStore.entities.FilmeAtor;
import br.com.dextraining.mediaStore.services.AtorService;
import br.com.dextraining.mediaStore.services.CategoriaService;
import br.com.dextraining.mediaStore.services.FilmeService;

public class FilmeServlet extends HttpServlet{

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String valor = req.getParameter("valor");
		String descricao = req.getParameter("descricao");
		String diretor = req.getParameter("diretor");
		
		String[] categoriasString = req.getParameterValues("categorias");
		List<Categoria> categorias = construirListaDeCategorias(categoriasString);
		
		Filme filme = new Filme();
		
		filme.setValor(new BigDecimal(valor));
		filme.setDescricao(descricao);
		filme.setDiretor(diretor);
		filme.setCategorias(categorias);
		
		String[] idsAtores = req.getParameterValues("atores"); 
		constroiFilmeAtores(filme, idsAtores);
		
		FilmeService filmeService = new FilmeService();
		filmeService.persist(filme);
		resp.sendRedirect("/sucesso.jsp");
	}

	private void constroiFilmeAtores(Filme filme, String[] idsAtores) {
		List<FilmeAtor> atores = new ArrayList<FilmeAtor>();
		for(String idString : idsAtores) {
			AtorService atorService = new AtorService();
			Ator ator = atorService.findById(Long.valueOf(idString));
			FilmeAtor filmeAtor = new FilmeAtor();
			filmeAtor.setAtor(ator);
			filmeAtor.setFilme(filme);
			filmeAtor.setPapel(ator.getNome());
			atores.add(filmeAtor);
		}
		filme.setFilmeAtores(atores);
	}

	private List<Categoria> construirListaDeCategorias(String[] categoriasString) {
		List<Categoria> categorias = new ArrayList<Categoria>();
		CategoriaService categoriaService = new CategoriaService();
		System.out.println("Categorias:" + categoriasString);
		for(String id : categoriasString) {
			categorias.add(categoriaService.findById(Long.valueOf(id)));
		}
		return categorias;
	}
}
