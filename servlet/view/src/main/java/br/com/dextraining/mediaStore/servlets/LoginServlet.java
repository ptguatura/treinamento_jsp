package br.com.dextraining.mediaStore.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginServlet extends HttpServlet{

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String login = req.getParameter("login");
		String senha = req.getParameter("senha");
		if(login != null && login.equals("admin") 
				&& senha != null && senha.equals("123")){
			req.getSession().setAttribute("login", login);
			resp.sendRedirect("/secure/index.jsp");
		} else {
			resp.sendRedirect("/error/login-invalido.jsp");
		}
	}
}
