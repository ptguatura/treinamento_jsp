<html>
	<head>
		<title>Media Store - Cadastro de Categoria</title>
		<%@include file="../../include.jsp" %>
	</head>
	<body>
		<h2>Cadastro de Categoria</h2>
		<%@include file="../menu.jsp" %>
		<form action="/s/categoria" method="post" class="pure-form pure-form-aligned" >
			<label>Nome:</label><input type="text" name="nome-categoria"/> <br/>
			<input type="submit" value="Cadastrar" class="pure-button pure-button-primary">
		</form>
	</body>
</html>