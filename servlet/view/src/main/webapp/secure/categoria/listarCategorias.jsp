<%@page import="br.com.dextraining.mediaStore.entities.Categoria"%>
<%@page import="java.util.List"%>
<%@page import="br.com.dextraining.mediaStore.services.CategoriaService"%>
<html>
<head>
<title>Listagem de Categorias</title>
		<%@include file="../../include.jsp" %>
</head>
<body>
	<h2>Listagem de Categorias</h2>
		<%@include file="../menu.jsp" %>
	<table class="pure-table pure-table-horizontal">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nome</th>
			</tr>
		</thead>
		<tbody>
			<%
			CategoriaService categoriaService = new CategoriaService();
				List<Categoria> categorias = categoriaService.findAll();
				for (Categoria cat : categorias) {
			%>
			<tr>
				<td><%=cat.getId()%></td>
				<td><%=cat.getNome()%></td>
			</tr>
			<%
				}
			%>
		</tbody>
	</table>
</body>
</html>
