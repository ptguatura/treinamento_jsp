<%@page import="br.com.dextraining.mediaStore.entities.Ator"%>
<%@page import="java.util.List"%>
<%@page import="br.com.dextraining.mediaStore.services.AtorService"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<html>
<head>
<title>Listagem de Atores</title>
<%@include file="../../include.jsp"%>
</head>
<body>
	<h2>Listagem de Atores</h2>
	<c:set var="teste" scope="page" value="Valor no escopo Page"/>
	<c:set var="teste" scope="request" value="Valor no escopo Resquest"/>
	<c:set var="teste" scope="session" value="Valor no escopo Session"/>
	<c:set var="teste" scope="application" value="Valor no escopo application"/>
	<%@include file="../menu.jsp"%>
	<%
		List<Ator> atores = new AtorService().findAll();
		pageContext.setAttribute("atores", atores);
	%>
	<table class="pure-table pure-table-horizontal">
		<thead>
			<tr>
				<th>ID</th>
				<th>Nome</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="ator" items="${atores}">
				<tr>
					<td><c:out value="${ator.id}"/></td>
					<td><c:out value="${ator.nome}"/></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>
