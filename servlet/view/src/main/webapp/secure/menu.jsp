<%@ taglib prefix="ex" uri="dextraTags"%>
<div class="pure-menu pure-menu-open pure-menu-horizontal">
	<ul>
		<li class="pure-menu-selected"><a href="/secure/index.jsp">P&aacute;gina Inicial</a></li>
		<li><a href="/secure/ator/inclusaoAtor.jsp">Incluir Ator</a></li>
		<li><a href="/secure/ator/listarAtores.jsp">Listar Atores</a></li>
		<li><a href="/secure/categoria/inclusaoCategoria.jsp">Incluir Categoria</a></li>
		<li><a href="/secure/categoria/listarCategorias.jsp">Listar Categoria</a></li>
		<li><a href="/secure/filme/inclusaoFilme.jsp"><ex:I18n message="filmes.inclusaoFilme" /></a></li>
	</ul>
</div>