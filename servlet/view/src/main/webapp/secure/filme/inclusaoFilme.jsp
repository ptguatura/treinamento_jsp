<%@page import="br.com.dextraining.mediaStore.services.AtorService"%>
<%@page import="br.com.dextraining.mediaStore.entities.Ator"%>
<%@page import="br.com.dextraining.mediaStore.services.CategoriaService"%>
<%@page import="br.com.dextraining.mediaStore.entities.Categoria"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<html>
	<head>
		<title>Media Store - Cadastro de Filmes</title>
		<%@include file="../../include.jsp" %>
	</head>
	<body>
		<h2>Cadastro de filmes</h2>
		<%@include file="../menu.jsp" %>
		<%
			List<Categoria> categorias = new CategoriaService().findAll();
			pageContext.setAttribute("categorias",categorias);
			List<Ator> atores = new AtorService().findAll();
			pageContext.setAttribute("atores",atores);
		%>
		<form action="/s/filme" method="post" class="pure-form pure-form-aligned">
		<div id="content">
			<label>Descricao:</label><input type="text" name="descricao"/> <br/>
			<label>Valor:</label><input type="text" name="valor"/> <br/>
			<label>Diretor:</label><input type="text" name="diretor"/> <br/>
			<label>Categorias:</label>
			<select name="categorias" multiple="multiple">
				<c:forEach var="categoria" items="${categorias}">
					<option value="<c:out value="${categoria.id}"/>"><c:out value="${categoria.nome}"/></option>
				</c:forEach>
			</select> 
			 <br/>
			 <label>Atores:</label>
			<select name="atores" multiple="multiple">
				<c:forEach var="ator" items="${atores}">
					<option value="<c:out value="${ator.id}"/>"><c:out value="${ator.nome}"/></option>
				</c:forEach>
			</select> 
			 <br/>
			<input id="cadastrar" type="submit" value="Cadastrar" class="pure-button">
		</div>
		</form>
	</body>
</html>