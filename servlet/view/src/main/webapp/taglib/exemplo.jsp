<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<html>
<head>
<title>TAGLIB - Escopos</title>
</head>
<body>
	<h2>Escopo de variaveis JSTL:</h2>
	<c:set var="teste" scope="page" value="Valor no escopo Page" />
	<c:set var="teste" scope="request" value="Valor no escopo Resquest" />
	<c:set var="teste" scope="session" value="Valor no escopo Session" />
	<c:set var="teste" scope="application"
		value="Valor no escopo application" />
	<c:out value="${teste}" />

	<table>
		<tr>
			<th>Escopo</th>
			<th>Valor</th>
		</tr>
		<tr>
			<td>Default</td>
			<td><c:out value="${teste}" /></td>
		</tr>
		<tr>
			<td>Page</td>
			<td><c:out value="${pageScope.teste}" /></td>
		</tr>
		<tr>
			<td>Request</td>
			<td><c:out value="${requestScope.teste}" /></td>
		</tr>
		<tr>
			<td>Session</td>
			<td><c:out value="${sessionScope.teste}" /></td>
		</tr>
		<tr>
			<td>Application</td>
			<td><c:out value="${applicationScope.teste}" /></td>
		</tr>
	</table>
</body>
</html>