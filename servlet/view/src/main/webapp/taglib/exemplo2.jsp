<%@page import="br.com.dextraining.mediaStore.services.CategoriaService"%>
<%@page import="br.com.dextraining.mediaStore.entities.Categoria"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<html>
<head>
	<title>TAGLIB - </title>
</head>
<body>
<%
	List<String> lista = new ArrayList<String>();
	lista.add("String 1");
	lista.add("String 2");
	lista.add("String 3");
	pageContext.setAttribute("lista",lista);
	pageContext.setAttribute("valor",330);
	Categoria categoria = new CategoriaService().findById(1L);
	
	pageContext.setAttribute("categoria", categoria);
%>
<c:if test="${valor != 50}" >
	<c:out value="${valor}"/>
</c:if>
<c:remove var="valor" />
<hr/>
<c:choose >
	<c:when test="${valor < 100}">Menor</c:when>
	<c:when test="${valor > 200}">Maior</c:when>
	<c:otherwise>Default</c:otherwise>
</c:choose>
<hr/>
<c:forEach var="meuTextoQuerido" items="${lista}">
	<c:out value="${meuTextoQuerido}" />
</c:forEach>
<hr/>
<c:out value="${categoria.nome}"/> <br/>
<c:out value="${categoria.id}"/>
<hr/>
</body>
</html>