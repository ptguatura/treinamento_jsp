<%@ tag import="br.com.dextraining.mediaStore.messages.MessagesUtils" import="java.text.DateFormat"%>
<%@ attribute name="message" required="true" %>
<%= MessagesUtils.getString(message) %>
