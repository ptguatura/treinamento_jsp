package br.com.paula.treinamento;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloWorldServlet extends HttpServlet {
	
	private static final long serialVersionUID = -6941261885287346386L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		StringBuffer buffer = new StringBuffer();
		buffer.append("<html>\n");
		buffer.append("<head><title>Título: Hello World</title></head>\n");
		buffer.append("<body>\n");
		buffer.append("<h1>Hello World!</h1>\n");
		buffer.append("</body>\n</html>");
		out.println(buffer.toString());
	}

}
